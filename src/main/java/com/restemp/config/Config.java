package com.restemp.config;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;

@Configuration
public class Config {

    @Bean
    ModelMapper getMapperObject() {
	return new ModelMapper();
	}
    
    @Value("${addressservice.base.url}")
    private String addressURL;
    
    //This piece of code to be not used because directly we are injecting 
    //the Template object through a template builder when service class loads
	/*
	 * @Bean RestTemplate getTemplate() { return new RestTemplate(); }
	 */
    
    @Bean
    WebClient webClient() {
    	return WebClient.builder().baseUrl(addressURL).build();
    }
}
