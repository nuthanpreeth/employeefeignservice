package com.restemp.response;

import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;

@Component
@Setter
@Getter
public class EmployeeResponse {
	private int id;
	private String name;
	private String email;
	private String bloodgroup;
	private AddressResponse addressResponse;
	
}
