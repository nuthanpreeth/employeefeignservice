package com.restemp.feigninterface;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import com.restemp.response.AddressResponse;

//http://localhost:8082/address-app/api/get/2
@org.springframework.cloud.openfeign.FeignClient(name="abc",url="http://localhost:8082/address-app/api/")
public interface AddressClient {
	@GetMapping("/get/{id}")
	AddressResponse getAddressByEmpoyteeID(@PathVariable("id") int id);
}