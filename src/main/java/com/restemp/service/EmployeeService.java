package com.restemp.service;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;

import com.restemp.bean.EmployeeBean;
import com.restemp.feigninterface.AddressClient;
import com.restemp.repository.EmployeeRepository;
import com.restemp.response.AddressResponse;
import com.restemp.response.EmployeeResponse;

@Service
public class EmployeeService {

	/* Instance level variable will not be loaded since this is being used in Constructor class which executes first from all.
	 * @Value("${addressservice.base.url}") private String addressURL;
	 */
	
	@Autowired
	EmployeeRepository repository;
	
	@Autowired
	ModelMapper mapper;
	
	@Autowired
	EmployeeResponse response;
	
	@Autowired
	AddressResponse addressResponse;
	
	@Autowired
	WebClient webclient;
	
	@Autowired
	AddressClient addressclient;
	
	
	public void saveEmployee(EmployeeBean empObject) {
		System.out.println(empObject);
	repository.save(empObject);	
	}
	
	public EmployeeResponse getEmployeeByID(Integer ID) {
		EmployeeBean bean=repository.findById(ID).get();
		response = mapper.map(bean,EmployeeResponse.class);
		addressResponse=addressclient.getAddressByEmpoyteeID(ID);
		response.setAddressResponse(addressResponse);
		return response;
	}
}
