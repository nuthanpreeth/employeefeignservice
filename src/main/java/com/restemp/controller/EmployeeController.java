package com.restemp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.restemp.bean.EmployeeBean;
import com.restemp.response.EmployeeResponse;
import com.restemp.service.EmployeeService;


@RestController
//@RequestMapping("/emp")
public class EmployeeController {

	@Autowired
	EmployeeService empService;
	
	@PostMapping("/save")
	public ResponseEntity<String> postMethodName(@RequestBody EmployeeBean employee) {
		//TODO: process POST request
		empService.saveEmployee(employee);
		return new ResponseEntity<>("Employee Saved",HttpStatus.OK);
	}
	
	@GetMapping("/get/{id}")
	public ResponseEntity<EmployeeResponse> getEmployeeByID(@PathVariable(value="id") int id){
		return ResponseEntity.status(HttpStatus.OK).body(empService.getEmployeeByID(id));
	}
	
}
